use std::mem::transmute;
use std::slice;
use std::collections::HashMap;

pub fn get_args() -> HashMap<String, Vec<String>> {
	let mut args: HashMap<String, Vec<String>> = HashMap::new();
	let mut current = String::from("");
	args.insert(current.clone(), Vec::new());

	for arg in ::std::env::args() {
		if arg.starts_with("--") {
			let key = String::from(arg.split_at(2).1);
			args.insert(key.clone(), Vec::new());
			current = key;
		} else {
			args.get_mut(&current).unwrap().push(arg.clone());
		}
	}

	args
}

pub struct AudioPacket {
	pub ty: u8,
	pub id: u16,
	pub opt: u8,
	pub channels: u8,
	pub samplerate: u32,
	pub duration: u16,
	pub data: Vec<f32>
}

#[derive(Clone,Debug,PartialEq,Eq)]
pub enum PacketError {
	NoData,
	Invalid,
	Truncated(usize),
	UnknownType,
	Other,
}

impl AudioPacket {
	pub fn create_bytes(id: u16, opt: u8, channels: u8, samplerate: u32, data: Vec<f32>) -> Vec<u8> {
		unsafe {
			let mut bytes = Vec::with_capacity(11 + (data.len() * 4));
			let id_ptr: *const u8 = transmute(&id as *const u16);
			let samplerate_ptr: *const u8 = transmute(&samplerate as *const u32);
			let duration = (data.len() / channels as usize) as u16;
			let duration_ptr: *const u8 = transmute(&duration as *const u16);
	
			// ty
			bytes.push(0);
			// id
			bytes.push(*id_ptr);
			bytes.push(*id_ptr.offset(1));
			// opt
			bytes.push(opt);
			// channels
			bytes.push(channels);
			// samplerate
			bytes.push(*samplerate_ptr);
			bytes.push(*samplerate_ptr.offset(1));
			bytes.push(*samplerate_ptr.offset(2));
			bytes.push(*samplerate_ptr.offset(3));
			// duration
			bytes.push(*duration_ptr);
			bytes.push(*duration_ptr.offset(1));
			// data
			bytes.extend_from_slice(slice::from_raw_parts(transmute::<*const f32, *const u8>(data.as_ptr()), data.len() * 4));
			bytes
		}
		
	}

	pub fn from_slice(data: &[u8]) -> Result<AudioPacket, PacketError> {
		unsafe {
			if data.is_empty() {
				return Err(PacketError::NoData);
			}
			
			let ty = &data[0];
			
			match *ty {
				0 => {
					if data.len() < 11 {
						return Err(PacketError::Invalid);
					}
				
					let id: u16 = *transmute::<*const u8, *const u16>(data[1..3].as_ptr());
					let opt: u8 = data[3];
					let channels: u8 = data[4];
					let samplerate: u32 = *transmute::<*const u8, *const u32>(data[5..9].as_ptr()); 
					let duration: u16 = *transmute::<*const u8, *const u16>(data[9..11].as_ptr());
					let end = 11 + (channels as usize * duration as usize * 4);
				
					if data.len() < end {
						return Err(PacketError::Truncated(end - data.len()));
					}
					
					let data: &[f32] = slice::from_raw_parts(transmute(data[11..end].as_ptr()), channels as usize * duration as usize);
					
					Ok(AudioPacket {
						ty: *ty,
						id, opt, channels,
						samplerate, duration,
						data: data.to_vec()
					})
				},
				
				_ => return Err(PacketError::UnknownType)
			}
		}
	}
}
