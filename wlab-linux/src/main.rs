extern crate cpal;
extern crate wlab_common;
extern crate parking_lot;

use cpal::*;
use wlab_common::*;

use std::thread;
use std::net::UdpSocket;
use std::collections::VecDeque;
use parking_lot::Mutex;
use std::sync::Arc;
use std::collections::BTreeMap;
use std::net::SocketAddr;
use std::time::Instant;
use std::time::Duration;

struct Client {
	addr: SocketAddr,
	format: Format,
	stream_id: StreamId,
	recv_queue: BTreeMap<u16, Vec<f32>>,
	last_recv: u16,
	play_buffer: VecDeque<f32>,
	alive: bool,
	last_recv_inst: Instant,
}

fn main() {
	let args = Arc::new(get_args());
	let listen: SocketAddr = match args.get("listen") {
		Some(vals) => match vals.first() {
			Some(some) => match some.parse() {
				Ok(ok) => ok,
				Err(_) => {
					println!("Bad socket address.");
					return;
				}
			}, None => {
				println!("No socket address specified.");
				return;
			}
		}, None => {
			"0.0.0.0:30000".parse().unwrap()
		}
	};
	
	match args.get("help") {
		Some(_) => {
			println!("  --listen=[socketaddr]  Specify address to listen on. Default 0.0.0.0:30000");
			println!("  --show-latency         Caculate and print latency of clients periodically.");
			return;
		}, None => ()
	}
	
	let event_loop = Arc::new(EventLoop::new());
	let output_device = default_output_device().expect("no output device available");
	let clients: Arc<Mutex<Vec<Client>>> = Arc::new(Mutex::new(Vec::new()));
	let clients_cp = clients.clone();
	let event_loop_cp = event_loop.clone();
	
	thread::spawn(move || {
		let socket = UdpSocket::bind(listen).expect("Failed to bind");
		let mut recv_buffer = Vec::with_capacity(2048);
		recv_buffer.resize(2048, 0);
		
		'recv: loop {
			match socket.recv_from(recv_buffer.as_mut_slice()) {
				Ok((amt, addr)) => match AudioPacket::from_slice(&recv_buffer[0..amt]) {
					Ok(packet) => {
						let mut clients = clients_cp.lock();
						let packet_format = Format {
							channels: packet.channels as u16,
							sample_rate: SampleRate(packet.samplerate),
							data_type: SampleFormat::F32,
						};
					
						for client in &mut *clients {
							if client.addr == addr && client.alive {
								if client.format != packet_format {
									client.alive = false;
								} else {
									if packet.id == 0 || packet.id > client.last_recv {
										client.recv_queue.insert(packet.id, packet.data);
										client.last_recv_inst = Instant::now();
									} continue 'recv;
								}
							}
						}
						
						let stream_id = event_loop_cp.build_output_stream(&output_device, &packet_format).expect("failed to build output stream");
						let mut client = Client {
							addr: addr.clone(),
							format: packet_format.clone(),
							stream_id,
							recv_queue: BTreeMap::new(),
							alive: true,
							last_recv: 0,
							play_buffer: VecDeque::new(),
							last_recv_inst: Instant::now(),
						};
						
						client.recv_queue.insert(packet.id, packet.data);
						clients.push(client);
					}, Err(e) => match e {
						PacketError::Truncated(amt) => {
							println!("Failed to parse packet from {:?}, {:?}", addr, e);
							println!("Received truncated packet growing recv buf to {}.", recv_buffer.len() + amt);
							recv_buffer.resize(recv_buffer.len() + amt, 0);
						}, e => {
							println!("Failed to parse packet from {:?}, {:?}", addr, e);
						}
					}
				}, Err(e) => {
					println!("Failed to receive data from socket: {}", e);
				}
			}
		}
	});
	
	let event_loop_cp = event_loop.clone();
	let clients_cp = clients.clone();
	let args_cp = args.clone();
	
	thread::spawn(move || {
		loop {
			let mut clients = clients_cp.lock();
			let mut remove = Vec::new();
			
			for(i, client) in clients.iter_mut().enumerate() {
				if !client.alive || client.last_recv_inst.elapsed() > Duration::from_secs(10) {
					println!("Client {:?} has gone inactive.", client.addr);
					remove.push(i);
				} else {
					if args_cp.contains_key("show-latency") {
						let cpal::SampleRate(ref rate) = &client.format.sample_rate;
						let vps = *rate as usize * client.format.channels as usize;
						let mut len = client.play_buffer.len();
						
						for (_, data) in &client.recv_queue {
							len += data.len();
						}
						
						let ms = (len as f32 / vps as f32) * 1000.0;
						println!("Client {:?}: Latency {:.1} ms", client.addr, ms);
					}
				}
			}
			
			for i in remove.into_iter().rev() {
				let client = clients.swap_remove(i);
				event_loop_cp.destroy_stream(client.stream_id);
			}
			
			drop(clients);
			thread::sleep(Duration::from_secs(30));
		}
	});

	let clients_cp = clients.clone();
	
	event_loop.run(move |stream_id, stream_data| {
		match stream_data {
			StreamData::Output { buffer: UnknownTypeOutputBuffer::F32(mut buffer) } => {
				let mut clients = clients_cp.lock();
				
				for client in &mut *clients {
					if client.stream_id == stream_id {
						let mut zero = false;
					
						for v in &mut *buffer {
							*v = 'val: loop {
								match client.play_buffer.pop_front() {
									Some(some) => break some,
									None => if zero {
										for _ in 0..client.format.channels {
											client.play_buffer.push_back(0.0);
										}
									} else {
										if client.recv_queue.is_empty() {
											zero = true;
											continue 'val;
										}
										
										let keys: Vec<u16> = client.recv_queue.keys().cloned().collect();
										let mut wrap = false;
										let mut i = 0;
									
										if client.recv_queue.len() > 30 {
											let mut rm = Vec::new();
											
											loop {
												if i >= keys.len() {
													i = 0;
													wrap = true;
													continue;
												} if keys.len() - rm.len() <= 30 {
													break;
												} if !wrap {
													if keys[i] <= client.last_recv {	
														i += 1;
														continue;
													} else {
														rm.push(i);
													}
												} else {
													rm.push(i);
												} i += 1;
											}
											
											i = 0;
											wrap = false;
											println!("fast forwarding {} packets for client {:?}", rm.len(), client.addr);
											
											for i in rm {
												client.recv_queue.remove(&keys[i]).unwrap();
											}
										}
										
										let id = loop {
											if i >= keys.len() {
												i = 0;
												wrap = true;
											} if !wrap {
												if keys[i] > client.last_recv {
													break keys[i];
												}
											} else {
												break keys[i];
											} i += 1;
										};
										
										for v in client.recv_queue.remove(&id).unwrap() {
											client.play_buffer.push_back(v);
											client.last_recv = id;
										}
									}
								}
							};
						}
					}
				}
			}, _ => ()
		}
	});
}
