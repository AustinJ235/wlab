extern crate cpal;
extern crate wlab_common;
extern crate parking_lot;

use cpal::*;
use wlab_common::*;

use std::net::UdpSocket;
use std::time::Duration;
use std::thread;
use std::time::Instant;
use std::sync::Arc;
use parking_lot::Mutex;
use std::sync::atomic::{self,AtomicBool};
use std::sync::Barrier;
use std::net::SocketAddr;

fn main() {
	let args = get_args();
	let remote: SocketAddr = match args.get("server") {
		Some(vals) => match vals.first() {
			Some(some) => match some.parse() {
				Ok(ok) => ok,
				Err(_) => {
					println!("Bad server.");
					return;
				}
			}, None => {
				println!("No server specified.");
				return;
			}
		}, None => {
			"192.168.122.1:30000".parse().unwrap()
		}
	};
	
	match args.get("help") {
		Some(_) => {
			println!("--server=[socketaddr] Specify where to send audio to.");
			return;
		}, None => ()
	}
	
	
	let event_loop = Arc::new(EventLoop::new());
	let socket = UdpSocket::bind("0.0.0.0:0").unwrap();
	socket.set_write_timeout(Some(Duration::from_millis(50))).unwrap();
	
	let mut silent_stream: StreamId = unsafe { ::std::mem::uninitialized() };
	let mut loopback_stream: StreamId = unsafe { ::std::mem::uninitialized() };
	let event_loop_cp = event_loop.clone();
	let last_recv = Arc::new(Mutex::new(Instant::now()));
	let last_recv_cp = last_recv.clone();
	let flush = Arc::new(AtomicBool::new(true));
	let flush_cp = flush.clone();
	let flush_info: Arc<Mutex<(u8, u32)>> = Arc::new(Mutex::new((0, 0)));
	let flush_info_cp = flush_info.clone();
	let flush_barrier = Arc::new(Barrier::new(2));
	let flush_barrier_cp = flush_barrier.clone();
	
	thread::spawn(move || {
		loop {
			let device = default_output_device().expect("no output device available");
			let mut supported_formats_range = device.supported_output_formats()
				.expect("error while querying formats");
			let format = supported_formats_range.next()
				.expect("no supported format?!")
				.with_max_sample_rate();
			
			unsafe {
				::std::ptr::write(&mut silent_stream, event_loop_cp.build_output_stream(&device, &format).unwrap());
				::std::ptr::write(&mut loopback_stream, event_loop_cp.build_output_loopback_stream(&device, &format).unwrap());
			}
			
			event_loop_cp.play_stream(silent_stream.clone());
			event_loop_cp.play_stream(loopback_stream.clone());
			
			let cpal::SampleRate(ref rate) = &format.sample_rate;
			{ *flush_info_cp.lock() = (format.channels as u8, *rate as u32); }
			flush_barrier_cp.wait();
			println!("Capture Started. {:?}", format);
			thread::sleep(Duration::from_millis(500));
			
			loop {
				if last_recv_cp.lock().elapsed() > Duration::from_millis(250) {
					event_loop_cp.destroy_stream(silent_stream.clone());
					event_loop_cp.destroy_stream(loopback_stream.clone());
					println!("No data received in 250 ms, restarting capture.");
					flush_cp.store(true, atomic::Ordering::Relaxed);
					break;
				}
				
				thread::sleep(Duration::from_millis(250));
			}
		}
	});
	
	let mut send_buffer = Vec::new();
	let mut id = 0;
	let mut send_size = 0;
	let mut channels = 0;
	let mut samplerate = 0;
	
	event_loop.run(move |_, stream_data| {
		match stream_data {
			StreamData::Output { buffer: UnknownTypeOutputBuffer::F32(mut buffer) } => {
				for v in &mut *buffer {
					*v = 0.0;
				}
			},

			StreamData::Input { buffer: UnknownTypeInputBuffer::F32(buffer) } => {
				if flush.swap(false, atomic::Ordering::Relaxed) {
					flush_barrier.wait();
					send_buffer.clear();
					let info = flush_info.lock();
					channels = info.0;
					samplerate = info.1;
					send_size = channels as usize * samplerate as usize / 100;
					send_size -= send_size % channels as usize;
				}
				
				*last_recv.lock() = Instant::now();
			
				for v in &*buffer {
					send_buffer.push(*v);
					
					if send_buffer.len() == send_size {
						let bytes = AudioPacket::create_bytes(id, 0, channels, samplerate, send_buffer.split_off(0));
						id = id.wrapping_add(1);
					
						if let Err(e) = socket.send_to(bytes.as_slice(), &remote) {
							println!("Failed to send packet: {}", e);
						}
					}
				}
			}, _ => ()
		}
	});
}