// winapi = { version = "0.3", features = ["mmdeviceapi","winerror","combaseapi","objbase","audioclient","mmreg","audiosessiontypes","minwindef","basetsd","ksmedia","synchapi"] }
extern crate winapi;

use std::ptr;
use std::mem::transmute;
use std::mem::uninitialized;

use winapi::Interface;
use winapi::um::combaseapi::CoInitializeEx;
use winapi::um::objbase::COINIT_MULTITHREADED;
use winapi::um::mmdeviceapi::IMMDeviceEnumerator;
use winapi::um::winnt::HRESULT;
use winapi::um::combaseapi::CoCreateInstance;
use winapi::um::mmdeviceapi::CLSID_MMDeviceEnumerator;
use winapi::um::combaseapi::CLSCTX_ALL;
use winapi::um::mmdeviceapi::eRender;
use winapi::um::mmdeviceapi::eConsole;
use winapi::um::mmdeviceapi::IMMDevice;
use winapi::um::audioclient::IAudioClient;
use winapi::um::audioclient::IID_IAudioCaptureClient;
use winapi::shared::mmreg::WAVEFORMATEX;
use winapi::um::audiosessiontypes::AUDCLNT_SHAREMODE_SHARED;
use winapi::um::audiosessiontypes::AUDCLNT_STREAMFLAGS_LOOPBACK;
use winapi::um::audioclient::IAudioCaptureClient;
use winapi::shared::minwindef::BYTE;
use winapi::shared::minwindef::DWORD;
use winapi::shared::basetsd::UINT32;
use winapi::um::synchapi::CreateEventW;
use winapi::um::audioclient::IID_IAudioRenderClient;
use winapi::um::audioclient::IAudioRenderClient;
use winapi::um::audiosessiontypes::AUDCLNT_STREAMFLAGS_EVENTCALLBACK;

#[inline]
fn check_result(result: HRESULT) -> Result<(), std::io::Error> {
    if result < 0 {
        Err(std::io::Error::from_raw_os_error(result))
    } else {
        Ok(())
    }
}

fn main() {
	unsafe {
		let stop_signal = CreateEventW(ptr::null_mut(), 1, 0, ptr::null_mut());
		let receive_signal = CreateEventW(ptr::null_mut(), 0, 0, ptr::null_mut());
		assert!(!stop_signal.is_null());
		assert!(!receive_signal.is_null());
	
		check_result(CoInitializeEx(ptr::null_mut(), COINIT_MULTITHREADED)).unwrap();
		let mut enumerator: *mut IMMDeviceEnumerator = uninitialized();

		check_result(CoCreateInstance(
			&CLSID_MMDeviceEnumerator,
			ptr::null_mut(),
			CLSCTX_ALL,
			&IMMDeviceEnumerator::uuidof(),
			transmute(&mut enumerator),
		)).unwrap();
		
		let mut device: *mut IMMDevice = uninitialized();
		check_result((*enumerator).GetDefaultAudioEndpoint(eRender, eConsole, &mut device)).unwrap();
		assert!(!device.is_null());

		let mut audio_client: *mut IAudioClient = uninitialized();
		
		check_result((*device).Activate(
			&winapi::um::audioclient::IID_IAudioClient,
			CLSCTX_ALL,
			ptr::null_mut(),
			transmute(&mut audio_client)
		)).unwrap();
		
		assert!(!audio_client.is_null());
		//check_result((*audio_client).SetEventHandle(receive_signal)).unwrap();
		
		let mut default_period = 0;
		let mut minimum_period = 0;
		
		check_result((*audio_client).GetDevicePeriod(&mut default_period, &mut minimum_period)).unwrap();
		
		let mut default_format: *mut WAVEFORMATEX = uninitialized();
		check_result((*audio_client).GetMixFormat(&mut default_format)).unwrap();
		assert!(!default_format.is_null());
		
		check_result((*audio_client).Initialize(
			AUDCLNT_SHAREMODE_SHARED, 
			AUDCLNT_STREAMFLAGS_EVENTCALLBACK | AUDCLNT_STREAMFLAGS_LOOPBACK,
			50000000, 0, default_format, ptr::null()
		)).unwrap();
		
		//
		
		let mut render_client: *mut IAudioRenderClient = uninitialized();
		
		{
			let mut audio_client: *mut IAudioClient = uninitialized();
			
			check_result((*device).Activate(
				&winapi::um::audioclient::IID_IAudioClient,
				CLSCTX_ALL,
				ptr::null_mut(),
				transmute(&mut audio_client)
			)).unwrap();
			
			let mut default_format: *mut WAVEFORMATEX = uninitialized();
			check_result((*audio_client).GetMixFormat(&mut default_format)).unwrap();
			
			check_result((*audio_client).Initialize(
				AUDCLNT_SHAREMODE_SHARED, 
				0, 50000000, 0, default_format, ptr::null()
			)).unwrap();
			
			let mut buffer_size = 0;
			check_result((*audio_client).GetBufferSize(&mut buffer_size)).unwrap();
			
			check_result((*audio_client).GetService(
				&IID_IAudioRenderClient,
				transmute(&mut render_client)
			)).unwrap();
			
			let mut buffer: *mut BYTE = uninitialized();
			
			check_result((*render_client).GetBuffer(
				buffer_size,
				&mut buffer,
			)).unwrap();
			
			let as_bytes: *mut u8 = transmute(buffer);
			
			for i in 0..((*default_format).nBlockAlign as isize * buffer_size as isize) {
				*as_bytes.offset(i) = (i % 255) as u8;
			}
			
			check_result((*render_client).ReleaseBuffer(buffer_size, 0)).unwrap();
		}
		
		//
		
		let mut capture_client: *mut IAudioCaptureClient = uninitialized();
		
		check_result((*audio_client).GetService(
			&IID_IAudioCaptureClient,
			transmute(&mut capture_client)
		)).unwrap();
		
		assert!(!capture_client.is_null());
		check_result((*audio_client).SetEventHandle(receive_signal)).unwrap();
		check_result((*audio_client).Start()).unwrap();
		
		let mut buffer_size = 0;
		check_result((*audio_client).GetBufferSize(&mut buffer_size)).unwrap();
		
		let mut frames: *mut BYTE = uninitialized();
		let packet_size: *mut UINT32 = uninitialized();
		let frames_available: *mut UINT32 = uninitialized();
		let flags: *mut DWORD = uninitialized();
		
		let mut pos = 0;
		let mut ts = 0;
		
		loop {
			::std::thread::sleep(::std::time::Duration::from_millis(10));
			check_result((*capture_client).GetNextPacketSize(packet_size)).unwrap();
			
			while *packet_size != 0 {
				check_result((*capture_client).GetBuffer(
					&mut frames,
					frames_available,
					flags,
					&mut pos,
					&mut ts
				)).unwrap();
				
				if frames.is_null() {
					println!("null");
				} else if *frames_available <= 0 {
					println!("zero");
					check_result((*capture_client).ReleaseBuffer(*frames_available)).unwrap();
					break;
				} else {
					println!("frames_available: {}", *frames_available);
				}
				
				check_result((*capture_client).ReleaseBuffer(*frames_available)).unwrap();
				check_result((*capture_client).GetNextPacketSize(packet_size)).unwrap();
			}
		}
    }
}
